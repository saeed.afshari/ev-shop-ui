FROM ubuntu:18.04

ARG SRC_PATH=/usr/local/evshop_src
ARG DEST_PATH=/usr/local/ev-shop-ui

RUN mkdir $DEST_PATH

COPY . $SRC_PATH

WORKDIR $SRC_PATH

RUN apt-get update && apt-get -y install curl
RUN curl -sL https://deb.nodesource.com/setup_12.x
RUN apt-get -y install nodejs
RUN apt-get -y install nginx
RUN npm install @angular/cli@next --global
RUN ng test
RUN ng build --prod
RUN mv ./dist/ev-shop-ui/* $DEST_PATH
RUN rm -rf $SRC_PATH

RUN mv $DEST_PATH/* /usr/share/nginx/html

