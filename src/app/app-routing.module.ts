import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LayoutComponent} from './components/layout/layout.component';
import {ListOrderComponent} from './car-order/components/list-order/list-order.component';
import {OrderSummaryComponent} from './car-order/components/order-summary/order-summary.component';


const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'create-order',
                pathMatch: 'full',
            },
            {
                path: 'create-order',
                loadChildren: () => import('./car-order/car-order.module').then(m => m.CarOrderModule),
                pathMatch: 'full',
            },
            {
                path: 'order-detail/:id',
                loadChildren: () => import('./car-order/car-order.module').then(m => m.CarOrderModule),
                pathMatch: 'full',
                component: OrderSummaryComponent
            },
            {
                path: 'car-orders',
                loadChildren: () => import('./car-order/car-order.module').then(m => m.CarOrderModule),
                pathMatch: 'full',
                component: ListOrderComponent
            },
        ],
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
