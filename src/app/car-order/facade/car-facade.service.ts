import {Injectable} from '@angular/core';
import {CarService} from '../service/car.service';
import {CarOrder} from '../model/car-order';
import {BehaviorSubject, EMPTY} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Car} from '../model/car';
import {ExtraOption} from '../model/extra-option';
import {CarOrderCreate} from '../model/car-order-create';

@Injectable({
    providedIn: 'root'
})
export class CarFacadeService {

    carOrders$ = new BehaviorSubject<CarOrder[]>(null);
    carOrder$ = new BehaviorSubject<CarOrder>(null);
    cars$ = new BehaviorSubject<Car[]>(null);
    batterySizes$ = new BehaviorSubject<ExtraOption[]>(null);
    wheels$ = new BehaviorSubject<ExtraOption[]>(null);
    tires$ = new BehaviorSubject<ExtraOption[]>(null);

    carOrderCreated$ = new BehaviorSubject<CarOrder>(null);

    error$ = new BehaviorSubject<any>(null);

    constructor(private carService: CarService) {
    }

    public dispatchGetCarOrders(): void {
        this.carService.getCarOrders()
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(result => {
            this.carOrders$.next(result);
        });
    }

    public dispatchGetCars(): void {
        this.carService.getCars()
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(result => {
            this.cars$.next(result);
        });
    }

    public dispatchGetExtraOptions(): void {
        this.carService.getExtraOptions()
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(result => {
            this.batterySizes$.next(result.filter(word => word.extraOptionType === 'BATTERY_SIZE'));
            this.wheels$.next(result.filter(word => word.extraOptionType === 'WHEEL'));
            this.tires$.next(result.filter(word => word.extraOptionType === 'TIRE'));
        });
    }

    public dispatchGetCarOrderById(orderId: number): void {
        this.carService.getCarOrderById(orderId)
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(result => {
            this.carOrder$.next(result);
        });
    }

    public dispatchCreateCarOrder(carOrderCreate: CarOrderCreate) {
        this.carService.createCarOrder(carOrderCreate)
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(result => {
            this.carOrderCreated$.next(result);
        });
    }
}
