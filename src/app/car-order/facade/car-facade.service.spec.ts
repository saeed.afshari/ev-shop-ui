import {TestBed} from '@angular/core/testing';

import {CarFacadeService} from './car-facade.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('CarFacadeService', () => {
  let service: CarFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(CarFacadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
