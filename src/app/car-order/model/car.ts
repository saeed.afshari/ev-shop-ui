export interface Car {
    id: number;
    model: string;
    basePrice: number;
    basePriceCurrency: string;
}
