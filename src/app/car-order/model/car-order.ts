import {ExtraOption} from './extra-option';

export interface CarOrder {
    id: number;
    carId: number;
    customerName: string;
    carModel: string;
    orderNumber: string;
    description: string;
    created: string;
    totalPrice: number;
    totalPriceCurrency: string;
    extraOptions: ExtraOption[];
}
