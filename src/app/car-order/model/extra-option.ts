export interface ExtraOption {
    id: number;
    carId: number;
    name: string;
    description: string;
    extraOptionType: string;
    cost: number;
    costCurrency: string;
}
