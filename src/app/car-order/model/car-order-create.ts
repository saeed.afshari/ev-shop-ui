export interface CarOrderCreate {
    carId: number;
    customerName: string;
    description: string;
    extraOptions: number[];
}
