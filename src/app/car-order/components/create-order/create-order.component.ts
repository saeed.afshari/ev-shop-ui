import {Component, OnInit} from '@angular/core';
import {CarFacadeService} from '../../facade/car-facade.service';
import {CarOrderCreate} from '../../model/car-order-create';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.scss']
})
export class CreateOrderComponent implements OnInit {

  constructor(public carFacadeService: CarFacadeService) {
  }

  ngOnInit(): void {
    this.carFacadeService.dispatchGetCars();
    this.carFacadeService.dispatchGetExtraOptions();
  }

  submit(carOrderCreate: CarOrderCreate) {
    this.carFacadeService.dispatchCreateCarOrder(carOrderCreate);
  }
}
