import {Component, OnInit} from '@angular/core';
import {CarFacadeService} from '../../facade/car-facade.service';

@Component({
  selector: 'app-list-order',
  templateUrl: './list-order.component.html',
  styleUrls: ['./list-order.component.scss']
})
export class ListOrderComponent implements OnInit {

  constructor(public carFacadeService: CarFacadeService) {
  }

  ngOnInit(): void {
    this.carFacadeService.dispatchGetCarOrders();
  }

}
