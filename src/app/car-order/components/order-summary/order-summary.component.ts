import {Component, OnInit} from '@angular/core';
import {CarFacadeService} from '../../facade/car-facade.service';
import {ActivatedRoute} from '@angular/router';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.scss']
})
export class OrderSummaryComponent implements OnInit {

  constructor(public carFacadeService: CarFacadeService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.pipe(map(p => p.id)).subscribe(id => {
      this.carFacadeService.dispatchGetCarOrderById(id);
    });
  }

}
