import {Component, Input, OnInit} from '@angular/core';
import {CarOrder} from '../../model/car-order';

@Component({
  selector: 'app-order-summary-detail',
  templateUrl: './order-summary-detail.component.html',
  styleUrls: ['./order-summary-detail.component.scss']
})
export class OrderSummaryDetailComponent implements OnInit {

  @Input() carOrder: CarOrder;

  constructor() {
  }

  ngOnInit(): void {
  }

}
