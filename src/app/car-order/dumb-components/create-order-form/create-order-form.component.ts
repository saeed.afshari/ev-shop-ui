import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ExtraOption} from '../../model/extra-option';
import {Car} from '../../model/car';
import {CarOrderCreate} from '../../model/car-order-create';

@Component({
  selector: 'app-create-order-form',
  templateUrl: './create-order-form.component.html',
  styleUrls: ['./create-order-form.component.scss']
})
export class CreateOrderFormComponent implements OnInit, OnChanges {

  @Input() cars: Car[] = [];
  @Input() batterySizes: ExtraOption[] = [];
  @Input() wheels: ExtraOption[] = [];
  @Input() tires: ExtraOption[] = [];

  @Output() whenSubmitted: EventEmitter<CarOrderCreate> = new EventEmitter<CarOrderCreate>();

  public submitted = false;

  public totalPrice = '0';

  public carOrderForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.buildForm();
  }

  private buildForm(): void {
    this.carOrderForm = this.formBuilder.group({
      customerName: [
        '',
        [Validators.required, Validators.minLength(3), Validators.maxLength(255)]
      ],
      description: [
        '',
        [Validators.maxLength(500)]
      ],
      car: [
        '',
        [Validators.required]
      ],
      batterySize: [
        '',
        [Validators.required]
      ],
      wheel: [
        '',
        [Validators.required]
      ],
      tire: [
        '',
        [Validators.required]
      ],
    });
  }

  updatePrice() {
    const rawForm = this.carOrderForm.getRawValue();
    let totalPrice = 0;
    if (rawForm.car > 0) {
      totalPrice += this.cars.filter(item => item.id == rawForm.car)[0].basePrice;
    }
    if (rawForm.batterySize > 0) {
      totalPrice += this.batterySizes.filter(item => item.id == rawForm.batterySize)[0].cost;
    }
    if (rawForm.wheel > 0) {
      totalPrice += this.wheels.filter(item => item.id == rawForm.wheel)[0].cost;
    }
    if (rawForm.tire > 0) {
      totalPrice += this.tires.filter(item => item.id == rawForm.tire)[0].cost;
    }
    this.totalPrice = totalPrice + ' ' + this.wheels[0].costCurrency;
  }

  submit() {
    this.submitted = true;
    if (this.carOrderForm.valid) {
      const rawForm = this.carOrderForm.getRawValue();

      const carOrderCreate: CarOrderCreate = {
        carId: rawForm.car,
        customerName: rawForm.customerName,
        description: rawForm.description,
        extraOptions: [rawForm.batterySize, rawForm.wheel, rawForm.tire]
      };

      this.whenSubmitted.emit(carOrderCreate);
    }
  }
}
