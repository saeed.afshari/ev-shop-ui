import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {CarOrder} from '../../model/car-order';

@Component({
  selector: 'app-list-order-detail',
  templateUrl: './list-order-detail.component.html',
  styleUrls: ['./list-order-detail.component.scss']
})
export class ListOrderDetailComponent implements OnInit, OnChanges {
  @Input() carOrders: CarOrder[] = [];

  constructor() {
  }

  ngOnInit(): void {
    console.log(this.carOrders)
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.carOrders)
  }

}
