import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CarOrder} from '../model/car-order';
import {environment} from '../../../environments/environment';
import {Car} from '../model/car';
import {ExtraOption} from '../model/extra-option';
import {CarOrderCreate} from '../model/car-order-create';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(private httpClient: HttpClient) {
  }

  public getCarOrders(): Observable<CarOrder[]> {
    return this.httpClient.get<CarOrder[]>(`${environment.backendUrl}/car-order`);
  }

  public getCarOrderById(orderId: number): Observable<CarOrder> {
    return this.httpClient.get<CarOrder>(`${environment.backendUrl}/car-order/${orderId}`);
  }

  public getCars(): Observable<Car[]> {
    return this.httpClient.get<Car[]>(`${environment.backendUrl}/car`);
  }

  public getExtraOptions(): Observable<ExtraOption[]> {
    return this.httpClient.get<ExtraOption[]>(`${environment.backendUrl}/extra-option`);
  }

  public createCarOrder(carOrderCreate: CarOrderCreate) {
    return this.httpClient.post<CarOrder>(`${environment.backendUrl}/car/${carOrderCreate.carId}/order`, carOrderCreate);
  }
}
