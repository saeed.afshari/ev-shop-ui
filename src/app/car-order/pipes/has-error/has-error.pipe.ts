import {Pipe, PipeTransform} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Pipe({
  name: 'hasError'
})
export class HasErrorPipe implements PipeTransform {

  transform(formGroup: FormGroup, triggerUpdate: any, controlName: string, submitted: boolean): boolean {
    if (!formGroup) {
      return false;
    }
    const control = formGroup.controls[controlName];
    if (!control) {
      return false;
    }
    return (submitted || control.dirty) && !control.valid;
  }

}
