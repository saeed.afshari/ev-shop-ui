import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CarOrderRoutingModule} from './car-order-routing.module';
import {ListOrderComponent} from './components/list-order/list-order.component';
import {CreateOrderComponent} from './components/create-order/create-order.component';
import {CreateOrderFormComponent} from './dumb-components/create-order-form/create-order-form.component';
import {ListOrderDetailComponent} from './dumb-components/list-order-detail/list-order-detail.component';
import {OrderSummaryComponent} from './components/order-summary/order-summary.component';
import {HttpClientModule} from '@angular/common/http';
import {OrderSummaryDetailComponent} from './dumb-components/order-summary-detail/order-summary-detail.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HasErrorPipe} from './pipes/has-error/has-error.pipe';


@NgModule({
  declarations: [
    ListOrderComponent,
    CreateOrderComponent,
    CreateOrderFormComponent,
    ListOrderDetailComponent,
    OrderSummaryComponent,
    OrderSummaryDetailComponent,
    HasErrorPipe],
  imports: [
    CommonModule,
    CarOrderRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ]
})
export class CarOrderModule {
}
