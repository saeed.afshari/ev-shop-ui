import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {CarFacadeService} from './car-order/facade/car-facade.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'EV Shop';

    constructor(private toastr: ToastrService,
                private carFacadeService: CarFacadeService,
                private router: Router) {
    }

    ngOnInit(): void {
        this.carFacadeService.error$.subscribe(err => {
            if (err === null) {
                return;
            }
            if (err && err.error) {
                this.toastr.error(err.error.message);
                return;
            }
            this.toastr.error('Something went wrong! Please try again later.');
        });

        this.carFacadeService.carOrderCreated$.subscribe(carOrder => {
            if (carOrder === null) {
                return;
            }
            this.toastr.success('Your order submitted successfully');
            this.router.navigate([`order-detail/${carOrder.id}`]);
        });
    }
}
