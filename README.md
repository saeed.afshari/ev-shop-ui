# EvShopUi
Electronic Vehicle shop, frontend application.
You can get access to the project repository [here](https://gitlab.com/saeed.afshari/ev-shop-ui).

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.

## Deployment
In order to launch the application, execute the run.sh script.
```
sudo bash ./run.sh
```

## Requirements
As the application developed in Angular 9.1, the following dependencies are mandatory:
- NodeJs lts
- ng cli (Angular cli: you can install it with ```npm install @angular/cli@next --global```)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
